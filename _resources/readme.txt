/**************
* Project:  DMSF Student Online Directory
* @author:  Dindo Gatmatian <dindogatmaitan@gmail.com>
* @version: prototype 
* @date:    06/09/2016
/************


# Requirements: 
 PHP 5+, MySQL v5.5+ or MariaDB 5.3+

# Initial Setup:
 After extracting the site to your web root (ex. ../htdocs/dmsf/)

 Step 1 - Setup Database [PhpMyAdmin]
   1.1 Create a database on your mysql and name it dmsf_v1
   1.2 Import the file from ../dmsf/_resources/db_scripts/dmsf_v1.sql

 Step 2: Setup Local Configuration [root folder ../htdocs/dmsf/]
  [../.htaccess]
   2.1 On your root folder, using a text editor open the file ../default.htaccess
   2.2 Look for the line #RewriteBase and enter the correct path of your site. 
      - Ex: it should look something like this '#RewriteBase /dmsf/'
   2.3 Save the file ../dmsf/.htaccess 
   
  [../application/config/config.php]
   2.3 Next, open the file ../application/config/config_default.php
   2.4 Look for the line $config['base_url'] = ...
   2.5 Enter the correct path of your site.  
      - Ex: $config['base_url'] = 'http://'. $_SERVER['HTTP_HOST'] .'/dmsf/';
   2.6 Save the file as config.php at the same location where you opened the config_default.php
 
  [../application/config/database.php]
   2.7 Same folder as your config.php look for database_default.php and open it.
   2.8 Look for the line $db['default'] = array(...)
   2.9 Enter your correct username and password to your database.
     - Ex: 'username' => 'root', 
           'password' =>'your_root_password',
   2.10 Change the database to dmsf_v1 
     - Ex: 'database' => 'dmsf_v1',
     
You're done! Test the site on your browser 
  - http://localhost/dmsf
  
Contact me if you encountered any problems 
- Dindo <dindogatmaitan@gmail.com>