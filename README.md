# README #


### DMEMS Project ###

* Initial development for DMEMS project
* Version 1
* [https://dindo_g@bitbucket.org/dindo_g/dmsf.git)
* Project:  DMSF Student Online Directory
* @author:  Dindo Gatmatian <dindogatmaitan@gmail.com>
* @version: prototype 
* @date:    06/09/2016


# Requirements: 
 PHP 5+, MySQL v5.5+ or MariaDB 5.3+

# Initial Setup:
 After extracting the site to your web root (ex. ../htdocs/dmems/)

# Step 1 - Setup Database [PhpMyAdmin]
   1.1 Create a database on your mysql and name it dmems_v1
   1.2 Import the file from ../dmems/_resources/db_scripts/dmems_v1.sql

# Step 2: Setup Local Configuration [root folder ../htdocs/dmems/]
  [../.htaccess]
   2.1 On your root folder, using a text editor open the file ../default.htaccess
   2.2 Look for the line #RewriteBase and enter the correct path of your site. 
      - Ex: it should look something like this '#RewriteBase /dmems/'
   2.3 Save the file ../dmems/.htaccess 
   
# Step 3: Configuration
   [../application/config/config.php]
   3.1 Next, open the file ../application/config/config_default.php
   3.2 Look for the line $config['base_url'] = ...
   3.3 Enter the correct path of your site.  
      - Ex: $config['base_url'] = 'http://'. $_SERVER['HTTP_HOST'] .'/dmems/';
   3.4 Save the file as config.php at the same location where you opened the config_default.php
 
# Step 4: Database Configuration 
   [../application/config/database.php]
   4.1 Same folder as your config.php look for database_default.php and open it.
   4.2 Look for the line $db['default'] = array(...)
   4.3 Enter your correct username and password to your database.
     - Ex: 'username' => 'root', 
           'password' =>'your_root_password',
   4.4 Change the database to dmems_v1 
     - Ex: 'database' => 'dmems_v1',
     
You're done! Test the site on your browser 
  - http://localhost/dmems
  
Contact me if you encountered any problems 
- Dindo <dindogatmaitan@gmail.com>