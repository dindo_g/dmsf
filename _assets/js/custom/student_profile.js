/*
 * student_profile.js
 * Functions that contains methods for student profile page 
 * Author: Dindo Gatmaitan <dindogatmaitan@gmail.com>
 * Date: 07/27/2016
 */
$( function (){
	// Date picker
	$('.date-picker').datepicker();
	
	// Date picker for date of birth
	$('.date-picker-year').datepicker({
		changeYear: true,
		maxDate: "-15y",
		dateFormat: 'yy-mm-dd'
	});
});