// On page load
$(function (){
	// Prepare auto-suggest function for Find User
	auto_suggest_user();
	// Add listener for Link Accounts button
	bind_add_link();
	// Add listener for removing link accounts
	bind_remove_link();
});

/*This method clears the search result */
function reset_results() {
	$('#search-result').addClass('hidden');
	$('#input-user').val('');
}

/* This method adds a new linked account to the 
 * users list
*/
function add_user_link() {
	var new_user = $('#user-link-profile').html();
	$('#account-links .box-body').append('<div class="row link-user-wrapper">'+new_user+'</div>');
}

/* This method adds a listener to the remove link button
*/
function bind_remove_link () {
	
	$('.js-remove-link').click(function(){
		var result=confirm('Are you sure you want to remove this link?');
		// If user confirms remove link
		if(result) {
			var url = $(this).attr('data-url');
			var wrapper = $(this).parents('div.link-user-wrapper');
			
			$.ajax({ // Run remove link
			method: "POST",
			url: url,
			data: { id: $(this).attr('data-id') },
			dataType: 'JSON',
			success: function (data) { 
					// check result
					if (data.result==1) {
						wrapper.fadeOut();
					}
				}
			}); //End ajax
		}
		return false;	
	});
}

/* This method adds a listener to the Link Accounts button 
 * which triggers an ajax call to save the accounts
*/
function bind_add_link() {
	$('#js-add-link').click(function(){
		var url = $('#link-acct-form').attr('action');
		var user_id = $('#user-sel').val();
		
		$.ajax({
			method: "POST",
			url: url,
			data: { student_id: user_id, parent_id: $('#parent-acct').val() },
			dataType: 'JSON',
			beforeSend: function () { 
				// Check if a user was selected
				if(user_id==0) {
					// prompt no user selected
					return false;
				}
			},
			success: function (data) { 
				// check result of link
				if (data.result==1) {
					// add user to links
					add_user_link();
					// hide search result
					reset_results();
				}
			}
		}) 
			.done(function( res ) {
				alert( "Data Saved: " + res.message );
			});
	}); // End .click()
} 


/* This method runs the auto-suggest list when 
 * typing in the Find User text box
*/
function auto_suggest_user() {
	var url = $('#input-user').attr('data-url');
	var img_url = $('#search-result').attr('data-imgurl');
	
	$('#input-user').autocomplete({
		source: url,
		minLength: 2,
		focus: function( event, ui ) {
      $('#input-user').val(ui.item.label);
      return false;
    },
		select: function (event, ui) {
			$('#user-sel').val(ui.item.value); // set selected user id
			// Show selected user
			show_user(ui.item.value);
			
			return false;			
		}
	}); // End autocomplete()
}

/* This method displays the found user */
function show_user(user_id) {
	var url = $('#search-result').attr('data-lookup-url');
	$.ajax({
		method: "GET",
		url: url,
		data: { id: user_id },
		dataType: 'HTML',
		success: function (data) { 
			$('#search-result .box-body').html(data);
			$('#search-result').removeClass('hidden');
			bind_add_link();
		}
	}) 
	
	
		
}