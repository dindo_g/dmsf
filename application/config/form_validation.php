<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
  'login' => array(
    array(
      'field' => 'identity',
      'label' => 'Email',
      'rules' => 'required|valid_email'
     ),
    array(
      'field' => 'password',
      'label' => 'Password',
      'rules' => 'required'
     )
   ),
  'user_add' => array(
     array(
      'field' => 'email',
      'label' => 'Email',
      'rules' => 'required|valid_email' // !missing callback to validate existing record
     ),
     array(
      'field' => 'password',
      'label' => 'Password',
      'rules' => 'required'
     ),
     array(
      'field' => 'confirm_password',
      'label' => 'Confirm Password',
      'rules' => 'required|matches[password]'
     ),
		 array(
      'field' => 'first_name',
      'label' => 'First name',
      'rules' => 'required'
     ),
     array(
      'field' => 'last_name',
      'label' => 'Last name',
      'rules' => 'required'
     )
   ),
   'user_update' => array(
		 array(
      'field' => 'first_name',
      'label' => 'First name',
      'rules' => 'required'
     ),
     array(
      'field' => 'last_name',
      'label' => 'Last name',
      'rules' => 'required'
     )
   ),
	 'change_password' => array(
		 array(
      'field' => 'old',
      'label' => 'Password',
      'rules' => 'required'
     ),
		 array(
			'field' => 'new',
			'label' => 'New password',
			'rules' => 'required'
		 ),
		 array(
			'field' => 'new_confirm',
			'label' => 'Confirm password',
			'rules' => 'required|matches[new]'
		 ),
	 ),
	 'user_profile' => array(
		 array(
      'field' => 'first_name',
      'label' => 'First Name',
      'rules' => 'required'
     ),
		 array(
			'field' => 'last_name',
			'label' => 'Last Name',
			'rules' => 'required'
		 )
	 )
  // apply more rules here...
);