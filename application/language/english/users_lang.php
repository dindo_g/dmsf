<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Users Lang - English
*
* Author: Dindo Gatmaitan
*         dindogatmaitan@gmail.com
*         
* Created:  08/01/2016
*
* Description:  English language file for User messages and errors
*
*/

// Error 
$lang['user_err'] = '<strong>Sorry.</strong> A problem was encountered and failed to complete your request. Please try again.';

// User Links
$lang['user_linked'] = '<strong>Account Linked!</strong> You have added the account to your links.';
$lang['user_linked_err'] = '<strong>Sorry.</strong> A problem was encountered while linking your accounts, please try again.';
$lang['user_link_remove'] = '<strong>Success.</strong> User has been removed from your Links.';
