<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct() {
    parent::__construct();
		// Verify user is logged in
		parent::check_user();
  }
	
  # View home page
	public function index( ) { 
	  $content_data = array();
		
		$data = array(
			'message' => $this->session->flashdata('message'),
			'content' => $this->load->view ('contents/dashboard/index', $content_data, true)
		);

		$this->render('main', $data);
	}
}
