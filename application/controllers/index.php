<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends MY_Controller {

	function __construct() {
    parent::__construct();
  }
	
  # View home page
	public function index( ) { 
	  if (!$this->ion_auth->logged_in()) {
			// redirect them to the login page
			redirect('account/login', 'refresh');
		} else {
      redirect('dashboard', 'refresh');
      
    }
	}
  
  public function home () {
    $content_data = array();
    
		$data = array(
			'content' => $this->load->view ('contents/public/home', $content_data, true)
		);

		$this->render('login', $data);
  }
}
