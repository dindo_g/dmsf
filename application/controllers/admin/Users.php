<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {
  
  private $view_path = "contents/admin/users/";
  
	function __construct() {
		parent::__construct();
		//verify user is admin
		parent::check_admin();
		
    $this->load->model('Users_model', 'Users');
    $this->load->model('Groups_model', 'Groups');
	}
  // User list
	function index () {
    $filter = 'users_groups.group_id NOT IN (2)';
    
    $content_data = array(
      'message' => $this->session->flashdata('message'),
      'users'   => $this->Users->get_details($filter),
      'key'     => $this->config->item('encryption_key')
    );
    
		$data = array(
			'content' => $this->load->view ($this->view_path.'index', $content_data, TRUE)
		);

		$this->render('main', $data);
  }
  
  // log the user out
	function link_users ($url_id=NULL) {
		if(empty($url_id)) {
			redirect('admin/users', 'refresh');
		}
		//pr($this->session->userdata('identity'));
		// Prepare page content
		$content_data = array(
      'message' 			=> $this->session->flashdata('message'),
			'relationships' => get_options_from_file('relationships'),
      'user' 					=> element(0, $this->Users->get_details('users.id='.$this->encrypt->decode($url_id)))
    );
    
		$data = array(
			'content' => $this->load->view ($this->view_path.'link_users', $content_data, TRUE),
			'css'			=> array(VEN.'jquery-ui/jquery-ui.min.css'),
			'js' 			=> array(VEN.'jquery-ui/jquery-ui.min.js', JS.'custom/users.js')
		);

		$this->render('main', $data);
	}
	// This method serves as a lookup for user through name or email
	function lookup_users () {
		$key = $this->input->get('term');
		
		$filter = "(users.first_name LIKE '$key%') OR (users.last_name LIKE '$key%')";
		
		if (strlen($key)>4) {
			$filter .= " OR (users.email LIKE '$key%')";
		}
		
		$this->load->model('student_model', 'Student');
		$result = $this->Student->_lookup($filter, 'users.last_name DESC', 0, 15);
		
		if(!$result) {
			$result = array('value'=>0,'label'=>'The user you are looking for is not registered.');
		} 
		echo json_encode($result);
	}
}
