<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Students extends MY_Controller {
	
	private $view_path = "contents/students/";
	
	function __construct() {
    parent::__construct();
		// Verify user is logged in
		parent::check_user();
		
    $this->load->model('Student_model', 'Student');
  }
	
  # View home page
	public function index( ) { 
	  $student_filter = null;
    
    $content_data = array(
      'students' => $this->Student->get_all_students($student_filter),
      'key' => $this->config->item('encryption_key')
    );
    
		$data = array(
			'content' => $this->load->view ($this->view_path.'index', $content_data, true),
      'css'      => array(CSS.'dataTables.bootstrap.min.css'),
      'js'      => array(JS.'pages/students.js',JS.'jquery.dataTables.min.js',JS.'dataTables.bootstrap.min.js')
		);
		
    $this->render('main', $data);
	}
  
  public function search() {
    if(!$this->input->post('student_key')) {
      redirect('students', 'refresh');
    }
    
    $key = $this->input->post('student_key');
    
     
    $content_data = array(
      'search_key' => $key,
      'students' => $this->Student->get_all_students($student_filter)
    );
    
		$data = array(
			'content' => $this->load->view ($this->view_path.'search', $content_data, true),
      'css'      => array(CSS.'dataTables.bootstrap.min.css'),
      'js'      => array(JS.'pages/students.js',JS.'jquery.dataTables.min.js',JS.'dataTables.bootstrap.min.js')
		);

		$this->render('main', $data);
  }
  
  public function view ($url_id=NULL) {
    // Check parameter exists
    if(!empty($url_id)) {
      // Decode url_id
			$student_id = $this->encrypt->decode($url_id);
    } else {
			$student_id = $this->session->userdata('user_id');
			$url_id = $this->encrypt->encode($this->session->userdata('user_id'));
		}
    
    // Prepare Content
    $content_data = array(
      'student' => element(0,$this->Student->get_student_profile('users.id='.$student_id))
    );
    $content_data['student']['url_id'] = $url_id;
    // Render
		$data = array(
			'content' => $this->load->view ($this->view_path.'view', $content_data, true)
		);
		$this->render('main', $data);
  }
	
	public function edit ($url_id=NULL) {
    // Check parameter exists
    if(empty($url_id)) {
      redirect('students', 'refresh');
    }
    // Decode url_id
    $student_id = $this->encrypt->decode($url_id);
    
		// Prepare Content
    $content_data = array(
      'message' 			=> $this->session->flashdata('message'),
      'student' 			=> element(0,$this->Student->get_student_profile('users.id='.$student_id)),
			'religions'			=> get_options_from_file('religions'),
			'nationalities'	=> get_options_from_file('nationalities'),
			'countries'			=> get_options_from_file('countries')
    );
    $content_data['student']['url_id'] = $url_id;
    // Render
		$data = array(
			'content' => $this->load->view ($this->view_path.'edit', $content_data, true),
			'css'			=> array(VEN.'jquery-ui/jquery-ui.min.css'),
			'js' 			=> array(VEN.'jquery-ui/jquery-ui.min.js', JS.'custom/student_profile.js')
		);
		$this->render('main', $data);
  }
  
	public function update () {
		$id = $this->input->post('id');
		
		if ($this->form_validation->run('user_profile')) {
			$data = array(
				'first_name' 	=> $this->input->post('first_name'),
				'last_name' 	=> $this->input->post('last_name'),
				'middle_name' => $this->input->post('middle_name'),
				'gender' 			=> $this->input->post('gender'),
				'birth_date' 	=> date('Y-m-d', strtotime($this->input->post('birth_date'))),
				'contact_no' 	=> $this->input->post('contact_no'),
				'religion' 		=> $this->input->post('religion'),
				'nationality' => $this->input->post('nationality'),
				'street' 			=> $this->input->post('street'),
				'city' 				=> $this->input->post('city'),
				'country' 		=> $this->input->post('country'),
				'zip_code' 		=> $this->input->post('zip_code')
			);
			// Save data
			if ( $this->Student->save($data, $id) ) {
				redirect('students/view/'.$this->encrypt->encode($id), 'refresh');
			}
		} else {
			$this->session->set_flashdata('message', validation_errors());
		}
		redirect('students/edit/'.$this->encrypt->encode($id), 'refresh');
	}
}
