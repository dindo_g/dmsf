<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {
  
  private $view_path = "contents/users/";
  
	function __construct() {
		parent::__construct();
		// Verify user is logged in
		parent::check_user();
		
		$this->lang->load('users_lang');
		
    $this->load->model('Users_model', 'Users');
    $this->load->model('Users_Links_model', 'UserLinks');
    $this->load->model('Groups_model', 'Groups');
	}
  // User list
	function index () {
		//verify user is admin
		parent::check_admin();
		
    $filter = 'users_groups.group_id NOT IN (2)';
    
    $content_data = array(
      'message' => $this->session->flashdata('message'),
      'users'   => $this->Users->get_details($filter),
      'key'     => $this->config->item('encryption_key')
    );
    
		$data = array(
			'content' => $this->load->view ($this->view_path.'index', $content_data, TRUE)
		);

		$this->render('main', $data);
  }
  
  // log the user out
	function link_users ($url_id=NULL) {
		if(empty($url_id)) {
			redirect('users', 'refresh');
		}
		$user_id = $this->encrypt->decode($url_id);
		//pr($this->session->userdata('identity'));
		// Prepare page content
		$content_data = array(
      'message' 			=> $this->session->flashdata('message'),
			'relationships' => get_options_from_file('relationships'),
      'user' 					=> element(0, $this->Users->get_details('users.id='.$user_id)),
			'user_links'    => $this->UserLinks->get_links($user_id)
    );
    
		$data = array(
			'content' => $this->load->view ($this->view_path.'link_users', $content_data, TRUE),
			'css'			=> array(VEN.'jquery-ui/jquery-ui.min.css'),
			'js' 			=> array(VEN.'jquery-ui/jquery-ui.min.js', JS.'custom/link_users.js')
		);

		$this->render('main', $data);
	}
	
	public function add_link () {
		$data = array(
			'student_id' => $this->input->post('student_id'),
			'parent_id'	 => $this->input->post('parent_id')
		);
		
		$return['result'] = 0;
		$return['message'] = $this->lang->line('user_err');
		
		if($this->UserLinks->save($data)){
			$return['result'] = 1;
			$return['id'] = $this->UserLinks->__insertID;
			$return['message'] = $this->lang->line('user_linked');
		}
		
		echo json_encode($return);
		
	}
	
	public function remove_link () {
		$link_id = $this->encrypt->decode($this->input->post('id'));
		$return['result'] = 0;
		$return['message'] = $this->lang->line('user_err');
		
		if($this->UserLinks->remove($link_id)){
			$return['result'] = 1;
			$return['message'] = $this->lang->line('user_link_remove');
		}
		
		echo json_encode($return);
		
	}
	
	function show_user () {
		$user_id = $this->input->get('id');
		
		$content['user'] = $this->Users->find('id='.$user_id, "id, CONCAT(last_name,', ',first_name,' ',middle_name) as full_name, email, profile_img");
		
		$this->load->view('fragments/users/link_users', $content);
	}
	
	// This method serves as a lookup for user through name or email
	function lookup_users () {
		$key = $this->input->get('term');
		
		$filter = "(users.first_name LIKE '$key%') OR (users.last_name LIKE '$key%')";
		
		if (strlen($key)>4) {
			$filter .= " OR (users.email LIKE '$key%')";
		}
		
		$this->load->model('student_model', 'Student');
		$result = $this->Student->_lookup($filter, 'users.last_name DESC', 0, 15);
		
		if(!$result) {
			$result = array('value'=>0,'label'=>'The user you are looking for is not registered.');
		} 
		echo json_encode($result);
	}
}
