<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo CSS.'bootstrap.min.css'; ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo CSS.'AdminLTE.min.css'; ?>">
  <link rel="stylesheet" href="<?php echo CSS.'skins/skin-blue.min.css'; ?>">
  <link rel="stylesheet" href="<?php echo CSS.'main.css'; ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- CSS Group -->
	<?php foreach($css as $style): ?>
  <link href="<?php echo CSS . $style; ?>" type="text/css" rel="stylesheet" />
	<?php endforeach; ?>
</head>
<body>
  
  <?php echo $content; ?>

<!-- REQUIRED JS SCRIPTS -->
<!-- jQuery 2.2.0 -->
<script src="<?php echo JS . 'jquery.2.2.4.min.js'; ?>"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo JS . 'bootstrap.min.js'; ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo JS . 'app.min.js'; ?>"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
<?php foreach($js as $script): ?>
<script type="text/javascript" src="<?php echo JS . $script; ?>"></script>
<?php endforeach; ?>	
</body>
</html>