<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Link your Account</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="<?php echo site_url('admin/users'); ?>">User Accounts</a></li>
		<li class="active">Link Accounts</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<!-- My Account -->
		<div class="col-sm-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">My Account</h3>
				</div> <!-- /.panel-heading -->
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-3">
							<img src="<?php echo PROFILE.$user['profile_img']; ?>" class="img-responsive img-thumbnail">
						</div>
						<div class="col-sm-9">
							<h4><?php echo $user['last_name'].', '.$user['first_name'].' '.$user['middle_name']; ?></h4>
							<p><i class="fa fa-envelope-square"></i> <?php echo $user['email']; ?></p>
							<p><i class="fa fa-user"></i> <?php echo $user['group']; ?></p>
							</ul>
						</div>
					</div>
					<div class="row">
					  <div class="box col-sm-12">
						  <div class="box-header">
								<h5 class="box-title"><i class="fa fa-exchange"></i> Your Links</h5>
							</div>
							<div class="box-body">
								
							</div>
						</div>
					</div>
				</div> <!-- /.panel-body -->
			</div> <!-- /.panel -->
		</div>
		<!-- End My Account -->
		
		<!-- Find User -->
		<div class="col-sm-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Find User</h4>
				</div>
				<div class="panel-body">
					<div class="alert alert-info">
						<p><i class="fa fa-info-circle"></i> Linking your account to another (Student Account) will allow you to <b>view</b> his/her records.</p>
					</div>
					<div class="row"><div class="col-sm-12">
						<form id="link-acct-form" class="form-horizontal" action="<?php echo site_url('admin/users/add_link'); ?>" role="form">
							<div class="form-group" >
								<label for="input-user" class=""></label>
								<div class="col-sm-12">
									<input type="text" name="input-user" id="input-user" class="form-control" placeholder="Start typing the Email or Name (Lastname Firstname) your looking for..." data-url="<?php echo site_url('admin/users/lookup_users'); ?>">
									<input type="hidden" id="user-sel" name="user-sel" value="0"> 
								</div>
							</div> <!-- /.form-group -->
							<div class="row">
								<div id="search-result" class="box col-sm-12 hidden" data-imgurl="<?php echo PROFILE; ?>">
									<div class="box-body">
										<div class="row">
											<div class="col-sm-3">
												<img src="<?php echo PROFILE.'default_user.png'?>" class="img-thumbnail">
											</div>
											<div class="col-sm-9">
												<h5>Lastname, Firstname M.</h5>
												<p>email@email.com</p>
											</div>
										</div>
										<div class="text-right">
											<button class="btn btn-success btn-sm" id="js-add-link"><i class="fa fa-exchange"></i> Link Accounts</button>
											<button class="btn btn-primary btn-sm" id="js-request-link"><i class="fa fa-plus"></i> Send Request</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div></div> 
				</div> <!-- /.panel-body -->
			</div> <!-- /.panel -->
		</div> <!-- End /.col-sm-6 -->
		<!-- End Find User -->
	</div> <!-- End /.row -->		
</section> <!-- /.content -->
