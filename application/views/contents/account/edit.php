<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Account</h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-12"><div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title">Edit Account</h3>
    </div>
    <!-- /.box-header -->
    <div class="panel-body">
      <!-- Error prompt -->
      <?php if($message):  ?>
      <div class="row"><div class="col-md-8 col-md-offset-2 alert alert-danger">
        <?php echo $message; ?>
      </div></div>
      <?php endif; ?>
      
      <form id="form-user-add" action="<?php echo site_url('account/update'); ?>" method="POST" role="form">
        <div class="form-group">
          <label for="email">Email</label>
          <input id="email" name="email" type="text" class="form-control" placeholder="Email" value="<?php echo set_value('email', $user['email']); ?>" readonly>
          <input name="user_id" type="hidden" value="<?php echo $user['url_id']; ?>">
        </div>
        <hr/>
        <div class="form-group">
          <label for="first_name">First Name</label>
          <input id="first_name" name="first_name" type="text" class="form-control" placeholder="First Name" value="<?php echo set_value('first_name', $user['first_name']); ?>">
        </div>
        <div class="form-group">
          <label for="last_name">Last Name</label>
          <input id="last_name" name="last_name" type="text" class="form-control" placeholder="Last Name" value="<?php echo set_value('last_name', $user['last_name']); ?>">
        </div>
        <div class="form-group">
          <label for="group">Account</label>
          <?php echo form_dropdown('group', $group_options, set_value('group',$user['group_id']), array('id'=>"group", 'class'=>"form-control", 'readonly'=>$is_admin)); ?>
        </div>
        <div class="form-group">
          <label for="active">Status</label>
          <?php echo form_dropdown('active', array(0=>'Inactive',1=>'Active'), set_value('active',$user['active']), array('id'=>"active", 'class'=>"form-control", 'readonly'=>$is_admin)); ?>
        </div>
        <!-- Button -->
        <div class="text-right">
          <a href="<?php echo site_url('account/view/'.$user['url_id']); ?>" type="button" class="btn btn-default">Cancel</a>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
  </div></div> <!-- End .panel -->
</section>
<!-- /.content -->