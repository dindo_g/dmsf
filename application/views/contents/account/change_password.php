  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Account</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url(''); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <?php if($this->ion_auth->in_group('admin')): ?>
      <li href="<?php echo site_url('admin/users'); ?>">User Accounts</li>
      <?php endif; ?>
      <li><a href="<?php echo $return_url; ?>"><?php echo $user['first_name'].' '.$user['last_name']; ?></a></li>
      <li class="active">Change Password</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="col-sm-12"><div class="box">
      <div class="box-header">
        <h3 class="box-title"><a href="<?php echo $return_url; ?>"><?php echo $user['first_name'].' '.$user['last_name']; ?></a> > Change Password</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form method="POST" action="<?php echo site_url('account/change_password/'.$this->encrypt->encode($user['id'])); ?>" role="form">
          <?php if($message): ?>
					<!-- Message Prompt -->
					<div class="alert alert-info col-sm-10 col-sm-offset-1"><?php echo $message; ?></div>
					<?php endif; ?>
					<div class="form-group">
            <label for="old">Old Password</label>
            <input id="old" name="old" type="password" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="new">New Password</label>
            <input id="new" name="new" type="password" class="form-control" placeholder="">
          </div>
          <div class="form-group">
            <label for="new_confirm">Confirm Password</label>
            <input id="new_confirm" name="new_confirm" type="password" class="form-control" placeholder="">
          </div>
					<input type="hidden" name="user" value="<?php echo $user['id']; ?>">
          <div class="text-right">
            <a href="<?php echo $return_url; ?>" type="button" class="btn btn-default">Cancel</a>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div></div> <!-- End .panel -->
  </section>
  <!-- /.content -->