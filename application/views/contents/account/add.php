<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>User Accounts</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo site_url(''); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <?php if($this->ion_auth->in_group('admin')): ?>
		<li><a href="<?php echo site_url('admin/users'); ?>">User Accounts</a></li>
		<?php endif; ?>
    <li class="active">New Account</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="col-md-8 col-md-offset-2"><div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title">New Account</h3>
    </div>
    <!-- /.box-header -->
    <div class="panel-body">
      <?php if($message):  ?>
      <div class="row"><div class="col-md-8 col-md-offset-2 alert alert-danger">
        <?php echo $message; ?>
      </div>
      </div>
      <?php endif; ?>
      <form id="form-user-add" action="<?php echo site_url('account/register'); ?>" method="POST" role="form">
        <div class="form-group">
          <label for="email">Email</label>
          <input id="email" name="email" type="text" class="form-control" placeholder="Email">
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input id="password" name="password" type="password" class="form-control" placeholder="">
        </div>
        <div class="form-group">
          <label for="confirm_password">Confirm Password</label>
          <input id="confirm_password" name="confirm_password" type="password" class="form-control" placeholder="">
        </div>
        <hr/>
        <div class="form-group">
          <label for="first_name">First Name</label>
          <input id="first_name" name="first_name" type="text" class="form-control" placeholder="First Name">
        </div>
        <div class="form-group">
          <label for="last_name">Last Name</label>
          <input id="last_name" name="last_name" type="text" class="form-control" placeholder="Last Name">
        </div>
        <div class="form-group">
          <label for="group">User Role</label>
          <?php echo form_dropdown('group', $group_options, set_value('group'), array('id'=>"group", 'class'=>"form-control")); ?>
        </div>
        <!-- Button -->
        <div class="text-right">
        <a href="<?php echo site_url('admin/users'); ?>" type="button" class="btn btn-default">Cancel</a>
        <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div></div> <!-- End .panel -->
</section>
<!-- /.content -->