<!-- Content Wrapper. Contains page content -->
<div class="login-body">
	<div class="wrapper">
	  <!-- Main content -->
	  <section class="content">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<a href=""><img id="login-logo" src="<?php echo IMG."dmems-logo.png"; ?>" /></a>
			
			<div class="login-wrap">
				<h1>LOG-IN SCREEN <small></small></h1>
					<div id="login-form">
						<form method="post" action="<?php echo site_url('account/verify_login');?>">							
							<p>Please log-in with your credentials below.</p>
							
							<?php if ($message): ?>
							<!-- Error Prompt -->
							<div class="alert alert-danger alert-dismissable col-sm-11 col-sm-offset-1" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<?php echo $message; ?>
							</div>
							<?php endif; ?>
							
							<div class="form-group">
								<input name="identity" type="text" class="form-control" placeholder="Username" />
							</div>
							<div class="form-group">
								<input name="password" type="password" class="form-control" placeholder="Password" />
							</div>
							<!-- <div class="checkbox">
								<label>
									<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
								</label>
							</div> -->
							<button type="submit" class="btn btn-default"> <i class="glyphicon glyphicon-log-in"></i>  Login</button>
						</form>
					</div>
			</div>
		</div>		
		<div class="col-md-4"></div>
	  </section>
	  <!-- /.content -->
	</div>
</div>
<!-- /.content-wrapper -->