  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>View Account</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url(''); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <?php if($this->ion_auth->in_group('admin')): ?>
      <li><a href="<?php echo site_url('admin/users'); ?>">User Accounts</a></li>
      <?php endif; ?>
      <li class="active">View Account</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="col-sm-12"><div class="box">
      <div class="box-header">
        <h3 class="box-title">Account Details</h3>
        <div class="box-tools">
          <a href="<?php echo site_url('account/edit/'.$user['url_id']); ?>"><i class="fa fa-pencil"></i> Edit</a>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
			<?php if($message): ?>
			<div class="alert alert-info col-sm-10 col-sm-offset-1">
				<?php echo $message; ?>
			</div>
			<?php endif; ?>
      <dl class="dl-horizontal">
        <dt>Name</dt>
        <dd><?php echo $user['last_name'].', '.$user['first_name']; ?></dd>
        <dt>Email</dt>
        <dd><?php echo $user['email']; ?></dd>
        <dt></dt>
        <dd>
					<?php if( ($this->ion_auth->in_group('admin')) OR ($this->session->userdata('identity')==$user['email'])): ?>
					<a href="<?php echo site_url('account/change_password/'.$this->encrypt->encode($user['id'])); ?>" >Change Password</a>
					<?php endif; ?>
				</dd>
        <dt>Account</dt>
        <dd><?php echo $user['group']; ?></dd>
        <dt>Status</dt>
        <dd><?php echo ($user['active'])?'Active':'Inactive'; ?></dd>
      </dl>
      </div>
    </div></div> <!-- End .panel -->
  </section>
  <!-- /.content -->