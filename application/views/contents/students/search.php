<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Search Result
    <small><?php echo count($students); ?> record(s) found</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo site_url('students'); ?>">Students Masterlist</a></li>
    <li class="active">Search Result</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-header">
      You searched for: <b><?php echo $search_key; ?></b>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row"><div class="col-sm-12">
        <table class="table table-bordered table-striped" id="students-table" role="grid">
        <thead>
        <tr role="row">
          <th>Photo</th>
          <th>ID</th>
          <th>Lastname, Firstname</th>
          <th>Email</th>
          <th>Gender</th>
          <th>Nationality</th>
          <th>Religion</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($students as $val): ?>
          <tr>
            <td>
            <img src="<?php echo base_url('_resources/profile_img/default_user.png'); ?>" class="img-responsive">
            </td>
            <td><?php echo $val['id']; ?></td>
            <td><?php echo $val['last_name'].', '. $val['first_name'].' '.$val['middle_name']; ?></td>
            <td><?php echo $val['email']?></td>
            <td><?php echo $val['gender']; ?></td>
            <td><?php echo $val['nationality']; ?></td>
            <td><?php echo $val['religion']; ?></td>
            <td>
              <a href="<?php echo base_url('students/view/'.$val['id']); ?>" title="View Profile"><i class="fa fa-user"></i> </a>
               | 
              <a href="<?php echo base_url('students/edit/'.$val['id']); ?>" class="Edit"><i class="fa fa-pencil"></i> </a>
            </td>
          </tr>
          <?php endforeach; ?>
          </tbody>
      </table>
    </div></div>
    </div>
    <!-- /.box-body -->
  </div>
</section>
<!-- /.content -->