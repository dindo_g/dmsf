<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Edit Profile
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo site_url('students'); ?>">Students Masterlist</a></li>
    <li class="active">Edit Profile</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-header">
      Edit Profile: <a href="<?php echo site_url('students/view/'.$this->encrypt->encode($student['id'])); ?>"><?php echo $student['first_name'].' '.$student['last_name']; ?></a>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row"><div class="col-sm-12">
        <div id="edit-profile" class="">
          <form name="edit-profile-form" id="edit-profile-form" method="POST" action="<?php echo site_url('students/update'); ?>" role="form" >
						<?php if ($message): ?>
						<div class="alert alert-danger">
							<?php echo $message; ?>
						</div>
						<?php endif; ?>
						<div class="form-group">
							<label for="first_name">First Name</label>
							<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" value="<?php echo $student['first_name']; ?>">
						</div>
						<div class="form-group">
							<label for="last_name">Last Name</label>
							<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" value="<?php echo $student['last_name']; ?>">
						</div>
						<div class="form-group">
							<label for="middle_name">Middle Name</label>
							<input type="text" id="middle_name" name="middle_name" class="form-control" placeholder="Middle Name" value="<?php echo $student['middle_name']; ?>">
						</div>
						<div class="form-group">
							<label for="gender">Gender</label>
							<?php echo form_dropdown('gender', array('Male'=>'Male', 'Female'=>'Female'), $student['gender'], array('class'=>'form-control','id'=>'gender')); ?>
						</div>
						<div class="form-group">
							<label for="birth_date">Date of Birth</label>
							<input type="text" id="birth_date" name="birth_date" class="form-control date-picker-year" placeholder="Birthday" value="<?php echo $student['birth_date']; ?>">
						</div>
						<div class="form-group">
							<label for="contact_no">Contact No.</label>
							<input type="text" id="contact_no" name="contact_no" class="form-control" placeholder="Contact No." value="<?php echo $student['contact_no']; ?>">
						</div>
						<div class="form-group">
							<label for="religion">Religion</label>
							<?php echo form_dropdown('religion', $religions, $student['religion'], array('class'=>'form-control','id'=>'religion')); ?>
						</div>
						<div class="form-group">
							<label for="religion">Nationality</label>
							<?php echo form_dropdown('nationality', $nationalities, $student['nationality'], array('class'=>'form-control','id'=>'religion')); ?>
						</div>
						<div class="form-group">
							<label for="street">Street Address</label>
							<input type="text" name="street" id="street" class="form-control" placeholder="Street Address" value="<?php echo $student['street']; ?>">
						</div>
						<div class="form-group">
							<label for="city">City</label>
							<input type="text" name="city" id="city" class="form-control" placeholder="City Address" value="<?php echo $student['city']; ?>">
						</div>
						<div class="form-group">
							<label for="country">Country</label>
							<?php echo form_dropdown('country', $countries, $student['country'], array('class'=>'form-control','id'=>'country')); ?>
						</div>
						<div class="form-group">
							<label for="zip_code">Zip Code</label>
							<input type="text" name="zip_code" id="zip_code" class="form-control" placeholder="Zip Code" value="<?php echo $student['zip_code']; ?>">
						</div>
						<input type="hidden" name="id" value="<?php echo $student['id']; ?>" >
						<div class="text-right">
							<a href="<?php echo site_url('students/view/'.$this->encrypt->encode($student['id'])); ?>" type="button" class="btn btn-default">Cancel</a>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
        </div>
      </div></div> <!-- End #student-tab-content -->
    </div>
    <!-- /.box-body -->
  </div>
</section>
<!-- /.content -->