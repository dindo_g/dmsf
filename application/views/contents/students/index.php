<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Students Masterlist
    <small><?php echo count($students); ?> records found</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Students Masterlist</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title"><?php echo count($students); ?> Students Found</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row"><div class="col-sm-12">
        <table class="table table-bordered table-striped" id="students-table" role="grid">
        <thead>
        <tr role="row">
          <th>ID</th>
          <th>Photo</th>
          <th>Lastname, Firstname</th>
          <th class="col-sm-hidden">Email</th>
          <th>Gender</th>
          <th>Nationality</th>
          <th>Religion</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($students as $val): ?>
          <tr>
            <!-- Profile Img -->
            <td><?php echo $val['id']; ?></td>
            <td>
              <a href="<?php echo base_url('students/view/'.$this->encrypt->encode($val['id'])); ?>" title="View Profile"><img src="<?php echo base_url('_resources/profile_img/default_user.png'); ?>" class="img-responsive img-thumbnail"></a>
            </td>
            <!-- Student Name -->
            <td>
              <a href="<?php echo base_url('students/view/'.$this->encrypt->encode($val['id'])); ?>" title="View Profile"><?php echo $val['last_name'].', '. $val['first_name'].' '.$val['middle_name']; ?></a>
            </td>
            <td><?php echo $val['email']?></td>
            <td><?php echo $val['gender']; ?></td>
            <td><?php echo $val['nationality']; ?></td>
            <td><?php echo $val['religion']; ?></td>
            <td>
              <a href="<?php echo base_url('students/view/'.$this->encrypt->encode($val['id'])); ?>" title="View Profile"><i class="fa fa-user"></i> </a>
               | 
              <a href="<?php echo base_url('students/edit/'.$this->encrypt->encode($val['id'])); ?>" class="Edit"><i class="fa fa-pencil"></i> </a>
            </td>
          </tr>
          <?php endforeach; ?>
          </tbody>
      </table>
    </div></div>
    </div>
    <!-- /.box-body -->
  </div>
</section>
<!-- /.content -->