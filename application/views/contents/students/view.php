<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Student Profile
    <small><?php echo ucfirst($student['first_name']).' '.ucfirst($student['last_name']); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo site_url('students'); ?>">Students Masterlist</a></li>
    <li class="active">Student Profile</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="box">
    <div class="box-header">
      <div class="col-sm-3">
        <img src="<?php echo site_url('_resources/profile_img/'.$student['profile_img']); ?>" class="img-responsive">
      </div>
      <div class="col-sm-4"> 
        <h3 class="box-title">
          <?php echo ucfirst($student['first_name']).' '.ucfirst($student['last_name']); ?>
          <?php echo (!empty($student['middle_name']))?', '.ucfirst($student['middle_name']):''; ?></h3>
        <dl class="dl-horizontal">
          <dt>Gender</dt> 
          <dd><?php echo $student['gender']; ?></dd>
          <dt>Birthdate</dt> 
          <dd><?php echo date('M. d, Y',strtotime($student['birth_date'])); ?></dd>
					<dt>Contact No.</dt> 
          <dd><?php echo $student['contact_no']; ?></dd>
        </dl>
      </div>
      <div class="col-sm-5">
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <!-- Student Profile Tabs -->
      <div class="row"> <div class="col-sm-12">
        <ul id="student-tabs" class="nav nav-tabs" data-val="<?php echo $student['url_id']; ?>" data-url="<?php echo site_url('student/load_profile'); ?>">
          <li role="profile" class="active" ><a href="#">Profile</a></li>
          <li role="visa"><a href="#">VISA</a></li>
          <li role="dormitory"><a href="#">Dormitory</a></li>
          <li role="academics"><a href="#">Academics</a></li>
        </ul>
      </div></div> <!-- End #student-profile-tabs -->
      <!-- Tab Content -->
      <div class="row"><div class="col-sm-12">
        <div id="student-tab-content" class="">
          <!-- See [content/students/profile/$tab_role] -->
          <?php $this->load->view('contents/students/profile/index', $student); ?>
        </div>
      </div></div> <!-- End #student-tab-content -->
    </div>
    <!-- /.box-body -->
  </div>
</section>
<!-- /.content -->