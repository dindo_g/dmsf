
  <div id="box-profile" class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">Profile</h3>
      <div class="box-tools pull-right">
        <a href="<?php echo site_url('students/edit/'.$student['url_id']); ?>" class=""><i class="fa fa-pencil"></i> Edit</a>
      </div>
    </div> <!-- End .box-header -->
    <div class="box-body">
      <div class="col-sm-6">
        <dl class="dl-horizontal">
          <dt>Nationality</dt>
          <dd><?php echo $student['nationality']; ?></dd>
          <dt>Religion</dt>
          <dd><?php echo $student['religion']; ?></dd>
          <dt>Address</dt>
          <dd><?php echo $student['street']; ?></dd>
          <dt>City</dt>
          <dd><?php echo $student['city']; ?></dd>
          <dt>Country</dt>
          <dd><?php echo $student['country']; ?></dd>
          <dt>Zip Code</dt>
          <dd><?php echo $student['zip_code']; ?></dd>
        </dl>
      </div>
      <!-- 2nd column -->
      <div class="col-sm-6">
        <dl class="dl-horizontal">
          <dt>Father's Name</dt>
          <dd>name_of_father here<?php //echo $student['religion']; ?></dd>
          <dt>Mother's Name</dt>
          <dd>name_of_mother here</dd>
          <dt>Home Address</dt>
          <dd><?php echo $student['home_street']; ?></dd>
          <dt>Home City</dt>
          <dd><?php echo $student['home_city']; ?></dd>
          <dt>Home Country</dt>
          <dd><?php echo $student['home_country']; ?></dd>
          <dt>Home Zip Code</dt>
          <dd><?php echo $student['home_zip_code']; ?></dd>
        </dl>
      </div>
    </div>
  </div> <!-- End .box -->

  <div id="box-account" class="box box-primary">
    <div class="box-header">
      <h4 class="box-title">Account</h4>
      <div class="box-tools pull-right">
        <!-- <a href="<?php echo site_url('account/edit/'.$student['url_id']); ?>"><i class="fa fa-pencil"></i> Edit</a> -->
      </div>
    </div>
    <div class="box-body">
      <div class="col-sm-6">
        <dl class="dl-horizontal">
          <dt>Account ID</dt>
          <dd><?php echo $student['id']; ?></dd>
          <dt>Email</dt>
          <dd><?php echo $student['email']; ?></dd>
          <!--<dt>Password</dt>
					<dd><a href="<?php echo site_url('account/change_password/'.$student['url_id']); ?>">Change Password</a></dd> -->
        </dl>
      </div>
      <!-- 2nd column -->
      <div class="col-sm-6">
        <dl class="dl-horizontal">
          <dt>Account Type</dt>
          <dd><?php echo ucfirst($student['group_description']); ?></dd>
          <dt>Status</dt>
          <dd><?php echo ($student['active'])?'Active':'Inactive'; ?></dd>
        </dl>
      </div>
    </div>
  </div> <!-- End .box-account -->