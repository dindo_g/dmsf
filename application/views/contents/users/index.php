  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>User Accounts</h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active">User Accounts</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-sm-3 pull-right text-right">
        <!-- Single button -->
        <a href="<?php echo site_url('account/add'); ?>" type="button" class="btn btn-success btn-sm"><i class="fa fa-user-plus"></i> New User</a>
      </div>
    </div>
    <div class="box">
      <div class="box-header">
        <h3 class="box-title"><?php echo count($users); ?> Record(s) Found</h3>
        
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row"><div class="col-sm-12">
          <table class="table table-bordered table-striped" id="users-table" role="grid">
          <thead>
          <tr role="row">
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Last Login</th>
            <th>Actions</th>
          </tr>
          </thead>
          <tbody>
          <?php foreach ($users as $val): ?>
            <tr>
              <td><?php echo $val['id']; ?></td>
              <td><?php echo $val['last_name'].', '. $val['first_name'].' '.$val['middle_name']; ?></td>
              <td><?php echo $val['email']?></td>
              <td><?php echo $val['group']; ?></td>
              <td><?php echo (!empty($val['last_login'])) ? date('m-d-Y (H:ia)', $val['last_login']) : 'Never'; ?></td>
              <td>
								<div class="text-center">
                <a href="<?php echo base_url('account/view/'.$this->encrypt->encode($val['id'])); ?>" title="View Profile" role="button"><i class="fa fa-user"></i></a> 
								
								<a href="<?php echo base_url('account/edit/'.$this->encrypt->encode($val['id'])); ?>" title="Edit Account" role="button"><i class="fa fa-pencil"></i></a> 
								
								<?php if($val['group']!='Parent Account'): ?>
									<span class="text-muted"><i class="fa fa-exchange"></i></span> 
								<?php else: ?>
									<a href="<?php echo base_url('users/link_users/'.$this->encrypt->encode($val['id'])); ?>" title="Link User" class="" role="button"><i class="fa fa-exchange"></i></a> 
								<?php endif; ?>
								</div>
              </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
      </div></div>
      </div>
      <!-- /.box-body -->
    </div>
  </section>
  <!-- /.content -->