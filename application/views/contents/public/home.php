<!-- Content Wrapper. Contains page content -->
<div class="wrapper">
  <!-- Main content -->
  <section class="content">
    <div class="col-md-4 col-md-offset-4">
      <h1>Login <small></small></h1>
      <p>Enter your registered Email and password.</p>
      <div id="login-form">
        <form class="form" method="post" action="<?php echo site_url('account/login');?>">
          <div class="form-group">
            <label>Email 
              <input name="identity" type="email" class="form-control" placeholder="Email" />
            </label>
          </div>
          <div class="form-group">
            <label>Password 
              <input name="password" type="password" class="form-control" placeholder="password" />
            </label>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox"> Remember me
            </label>
          </div>
          <button type="submit" class="btn btn-default">Login</button>
        </form>
      </div>
    </div>
  </section>

  
  <section class="content">

    <!-- Your Page Content Here -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->