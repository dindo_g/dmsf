<div id="user-link-profile" class="row">
	<div class="col-sm-3">
		<img src="<?php echo PROFILE.$user['profile_img']; ?>" class="img-thumbnail">
	</div>
	<div class="col-sm-9">
		<h5><?php echo $user['full_name']; ?></h5>
		<p><?php echo $user['email']; ?></p>
		<p>
			<a href="<?php echo site_url('students/view/'.$this->encrypt->encode($user['id'])); ?>" target="_blank">View Profile</a> 
			<button type="button" class="js-remove-link hidden" data-url="<?php echo site_url('users/remove_link'); ?>" data-id="0">Remove Link</button>
		</p>
	</div>
</div>
<div class="text-right">
	<button type="button" class="btn btn-success btn-sm" id="js-add-link"><i class="fa fa-exchange"></i> Link Accounts</button>
	<button type="button" class="btn btn-primary btn-sm" id="js-request-link"><i class="fa fa-plus"></i> Send Request</button>
</div>