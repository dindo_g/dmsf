<!-- Sidebar user panel (optional) -->
<div class="user-panel">
	<div class="pull-left image">
		<img src="<?php echo PROFILE.$this->session->userdata('profile_img'); ?>" class="img-circle" alt="User Image">
	</div>
	<div class="pull-left info">
		<p><?php echo $this->session->userdata('log_name'); ?></p>
		<!-- Status -->
		<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
	</div>
</div>

<!-- search form (Optional) -->
<form action="#" method="get" class="sidebar-form">
	<div class="input-group">
		<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
					</button>
				</span>
	</div>
</form>
<!-- /.search form -->

<ul class="sidebar-menu">
  <li class="active"><a href="<?php echo site_url('dashboard'); ?>"><i class="fa fa-link"></i> <span>Dashboard</span></a></li>
  <li ><a href="<?php echo site_url('students'); ?>"><i class="fa fa-link"></i> <span>Student Masterlist</span></a></li>
  <li class="header">Administrator</li>
  <!-- Optionally, you can add icons to the links -->
  <li class="treeview">
    <a href="#"><i class="fa fa-link"></i> <span>User Accounts</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href="<?php echo site_url('account/add'); ?>">Create Account</a></li>
      <li><a href="<?php echo site_url('users'); ?>">View List</a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="<?php echo site_url('dormitories'); ?>"><i class="fa fa-link"></i> <span>Dormitories</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href="#">Check-in/Check-out</a></li>
      <li><a href="#">Rooms</a></li>
      <li><a href="#">Reports</a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="<?php echo site_url('academics'); ?>"><i class="fa fa-link"></i> <span>Academics</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
      <li><a href="<?php echo site_url('academics'); ?>">View List</a></li>
    </ul>
  </li>
</ul>