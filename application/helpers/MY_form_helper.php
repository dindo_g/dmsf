<?php
  
  /*
   * Function: create_options() 
   * This function returns a formatted array for the 
   * CI form_dropdown().
   *
   * @param1[array] - the row or data of options
   * @param2[string] - the field name / column of the option value 
   * @param3[string] - the field name / column of the option label
   *
   * @Returns: array()
   *
   * Ex: create_options($data_rows, 'id', 'title');
   */
	function create_options ($array, $index, $value, $placeholder=FALSE) {
    $options = false;
    if ($placeholder) {
      $options[0] = '-- Select '.$placeholder.' --';
    }
    
    if (!empty($array)) {
      foreach ($array as $val) {
        $options[$val[$index]] = $val[$value];
      }
    }
    return $options;
  }
	
	function get_options_from_file ($filename='') {
		$options = FALSE; 
		
		$file = '_resources/'.$filename.'.txt';
		
		if(file_exists($file)) {
			$data = file_get_contents($file);
			$array = explode(',', $data);
		} 
		if(isset($array)) {
			foreach($array as $val) {
				$options[$val] = $val;
			}
		}
		
		return $options;
	}
  
  /*
   * Function: array_to_serial() 
   * This function returns a formatted array for the 
   * required format of JQuery $.autocomplete() 
   *
   * @param1[array] - the row or data of options
   * @param2[string] - the field name / column of the option label
   * @param3[string] - the field name / column of the option value 
   *
   * @Returns: array( array('label'=>@label, 'value'=>@value) )
   *
   * Ex: array_to_serial($data_rows, 'id', 'title');
   */
  function array_to_serial ($array, $value, $label) {
    $serial = false;
    if (!empty($array)) {
      foreach ($array as $val) {
        $suggestions[] = array('label' => $val[$label], 'value' => $val[$value]);
      }
    }
    return $serial;
  }
  
  /*
   * Function: getDateRangeOptions() 
   * This function returns a formatted array for the 
   * required format of JQuery $.autocomplete() 
   *
   * @param1[array] - the row or data of options
   * @param2[string] - the field name / column of the option value 
   * @param3[string] - the field name / column of the option label
   *
   * @Returns: array( array('label'=>@label, 'value'=>@value) )
   *
   * Ex: createSuggestions($data_rows, 'id', 'title');
   */
  function getDateRangeOptions() {
    $options = array(
      '0'           => '-- Set a Specific Date --',
      '1st Quarter' => '1st Quarter',
      '2nd Quarter' => '2nd Quarter',
      '3rd Quarter' => '3rd Quarter',
      '4th Quarter' => '4th Quarter',
      '1st Half'    => '1st Half',
      '2nd Half'    => '2nd Half',
      'annual'      => 'Annual'
    );
    return $options;
  }
  /*
   * Returns an array of the validation_errors set from CI
   */
  function validation_errors_array($prefix = '', $suffix = '') {
		
		if (FALSE === ($OBJ = & _get_validation_object())) {
			return '';
		}

		return $OBJ->error_array($prefix, $suffix);
	}
  
	function zero_fill_val($val, $zero_fill_len) {
		$strlen = strlen($val);
		$num_zeros = $zero_fill_len-$strlen;
		
		if($num_zeros<=1) 
		{
			return $val;
		
		} else {
			$zeros ='';
			for($i=0; $i<$num_zeros; $i++) {
				$zeros .=  '0';
			}
			
			return $zeros.$val;
		}
	}
	
	function getPrefixName()
	{
		
	}
	
