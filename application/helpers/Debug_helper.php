<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Checklist Helpers
 *
 * @package   G&G
 * @subpackage  Helpers
 * @category  Helpers
 * @author    Dido Gatmaitan <dindo@greyandgreentech.com>
 */


  function pr($debug, $exit=TRUE) {
    echo '<pre>';
    var_dump($debug);
    echo '</pre>';

    if ($exit) {
      exit;
    }
  }