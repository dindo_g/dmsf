<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Encrypt extension
*
* Version: 1
*
* Author: Dindo Gatmaitan
*		  dindogatmaitan@gmail.com
*
* Created:  07-21-2016
*
* Description:  Added encryption functionalities of current Encrypt class.
*
* Requirements: PHP5 or above
*/

class MY_Encrypt extends CI_Encrypt {

  function encode  ($string, $key='', $url_safe=TRUE) {
    $ret = parent::encode($string, $key);

    if ($url_safe) {
      $ret = strtr(
        $ret,
        array(
            '+' => '.',
            '=' => '-',
            '/' => '~'
        )
      );
    }
    return $ret;
  }

  function decode ($string, $key="") {
    $string = strtr(
      $string,
      array(
          '.' => '+',
          '-' => '=',
          '~' => '/'
      )
    );
    return parent::decode($string, $key);
  }
}