<?php


class Users_model extends MY_Model {
		function __construct() 	{
			parent::__construct();	
			$this->loadTable('users');
		}
    
    function get_details ($filter=NULL, $order_by=NULL, $limit=NULL, $offset=0) {
      $sql = "SELECT 
        users.*,
        groups.name AS 'group_name',
        groups.description AS 'group',
        users_groups.group_id
      FROM users
      LEFT JOIN 
        (users_groups, groups) ON (users_groups.user_id=users.id AND users_groups.group_id=groups.id)";
      
      if(!empty($filter)) {
        $sql .= " WHERE ".$filter;
      }
      
      if(!empty($order_by)) {
        $sql .= " ORDER BY ".$order_by;
      }
      
      return $this->db->query($sql)->result_array();
    }
    
    function set_group ($user_id, $group_id) {
      $sql = "UPDATE users_groups SET group_id=".$group_id." WHERE user_id=".$user_id;
      return $this->db->query($sql);
    }
}