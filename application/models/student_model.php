<?php


class Student_model extends MY_Model {
		function __construct() 	{
			parent::__construct();	
			$this->loadTable('users');
		}
    
  function get_all_students ($filter=NULL, $order_by=NULL) {
    $sql = "SELECT 
              users.*, 
              groups.name AS group_name, 
              groups.description AS group_description 
            FROM 
              users 
            LEFT JOIN 
              (users_groups, groups) 
              ON (users.id=users_groups.user_id AND groups.id=users_groups.group_id)
            WHERE
              users_groups.group_id=2";
    
    if(!empty($filter)) {
      $sql .= " AND " .  $filter;
    }
    
    $result = $this->db->query($sql)->result_array();

    return $result;
  }
  
  function get_student_profile ($filter=NULL, $order_by=NULL) {
    $sql = "SELECT 
              users.*, 
              groups.name AS group_name,
              groups.description AS group_description,
              users_groups.group_id 
            FROM 
              users 
            LEFT JOIN 
              (users_groups, groups) 
              ON (users.id=users_groups.user_id AND groups.id=users_groups.group_id)";
    
    if(!empty($filter)) {
      $sql .= " WHERE " .  $filter;
    }
    
    return $this->db->query($sql)->result_array();
  }
	
	function _lookup ($filter=NULL, $order_by=NULL, $offset=NULL, $limit=NULL) {
		$sql = "SELECT 
              users.id AS value, 
							CONCAT(users.last_name,', ',users.first_name,' ',users.middle_name) AS 'label', 
							users.email,
							users.profile_img AS 'icon'
            FROM 
              users 
            LEFT JOIN 
              (users_groups, groups) 
              ON (users.id=users_groups.user_id AND groups.id=users_groups.group_id)
            WHERE
              users_groups.group_id=2";
    
    if(!empty($filter)) {
      $sql .= " AND " . $filter;
    }
		if(!empty($order_by)) {
			$sql .= " ORDER BY ". $order_by;
		}
    // Apply limit
		if (!empty($limit)) {
			$sql .= " LIMIT $offset, $limit";
		}
    return $this->db->query($sql)->result_array();
	}
    
}