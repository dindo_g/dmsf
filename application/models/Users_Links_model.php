<?php


class Users_Links_model extends MY_Model {
		function __construct() 	{
			parent::__construct();	
			$this->loadTable('users_links');
		}
    
    function get_links ($parent_id) {
      $sql = "SELECT 
        users_links.*,
				CONCAT(students.last_name,', ',students.first_name,' ',students.middle_name) AS full_name,
				students.profile_img,
				students.email
      FROM users_links
      LEFT JOIN 
        users students ON (students.id=users_links.student_id)
			WHERE
				users_links.parent_id=".$parent_id;
      
      $sql .= " ORDER BY students.last_name";
      
      return $this->db->query($sql)->result_array();
    }
    
}