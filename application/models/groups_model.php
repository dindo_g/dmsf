<?php


class Groups_model extends MY_Model {
		function __construct() 	{
			parent::__construct();	
			$this->loadTable('groups');
		}
    
    function get_options (){
      return create_options($this->findAll(NULL, 'id,name, description'), 'id', 'description');
    }
}