<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
abstract class MY_Controller extends CI_Controller {
    
  protected $sections       = array();
  protected $additional_js  = array();
  protected $additional_css = array();
  protected $title          = 'DMEMS';
  protected $short_title    = 'DMEMS';
  protected $active_nav     = '';
  protected $fb_script      = false;
  
  public function __construct() {
    parent::__construct();
		
		//$this->output->enable_profiler(TRUE);
		
		$sections = array(
      'active_nav'    => $this->title,
      'title'         => $this->title,
      'short_title'   => $this->title,
      'fb_script'     => $this->fb_script,
      'content'       => null,
      'additional_js' => null,
      'additional_css'=> null,
		);
		$this->sections = $sections;
		//$this->add_js('jquery.price_format.1.8.min');
		//$this->add_js('common');
	}

  # Render function
	public function render($template_name, $sections = array()) {
    $sections['active_nav']     = $this->active_nav;
    $sections['title']          = $this->title;
    $sections['short_title']    = $this->short_title;
		$sections['js']             = (!empty($sections['js']))?array_merge($sections['js'], $this->additional_js):$this->additional_js;
    $sections['css']            = (!empty($sections['css']))?array_merge($sections['css'], $this->additional_css):$this->additional_css;
		$sections['fb_script']      = (!empty($sections['fb_script']))?$sections['fb_script']:$this->fb_script;
		$sections                   = array_merge($this->sections,$sections);
    
    $this->load->view('templates/'.$template_name, $sections);
	}
  
  public function set_title($title, $short_title = null) {
    $this->title = $title;
    $this->short_title = $title;
    if($short_title != null) {
      $this->short_title = $short_title;
    }
  }
  public function add_css($css, $no_cache=false) {
    if($no_cache) {
      $this->additional_css[] = $css.".css?".time();
    } else {
      $this->additional_css[] = $css.".css";
    }
  }
  public function add_js($js, $cache=true) {
        if($cache) {
            $this->additional_js[] = $js.".js";
        } else {
            $this->additional_js[] = $js.".js?".time();
        }
    }
    public function check_admin() {
        if (!$this->ion_auth->is_admin()) {
					$this->session->set_flashdata('message', $this->lang->line('access_denied'));
					redirect('dashboard','refresh');
				}
    }    
    public function check_user() {
        if (!$this->ion_auth->logged_in()) {
					$this->session->set_flashdata('message', $this->lang->line('login_timeout'));
					redirect('account/login', 'refresh');
				}
    }
		
    public function get_active_nav() {
        return $this->active_nav;
    }
    public function set_active_nav($nav) {
        $this->active_nav = strtolower($nav);
    }
}
# vim: ts=4 sw=4 smartindent
